#!/bin/bash
#Author	  : Purushothaman K
#Date 	  : Sep 28 2019, Saturday
#Purpose  : ssh jumper based on configuration.
#       	: To access a remote machine or execute a command at a remote machine, which is located at single or multiple hopes.
# 	      	: Terminal allocation and x11 connectivity also can be configured for the hopes.
#
#	REVISIONS/CHANGE LOG:
#		Feb 27 2020, Thursday
#			# Printing ssh path detail and/or command to be run with color.
#			# Showing ssh_conf name in the title bar (To know where we logged in current tab) : on login and logout
#
#			#Configuration handler fine tuned and simplified.	
#
#		Mar 13 2020, Friday
#			Added quiet option (Won't show connection closed message, will be helpful while getting only output of command.
#
#       May 12 2020, Tuesday
#           Added auto complete jumper name (while pressing TAB)
#   
#       Nov 6 2021 Saturday
#           Added clipboard copy feature (Useful for copying password into clipboard)
#           Added description field
#       
#       Feb 14 2022 Monday
#           Added option for passing port and identify key value for each hops
#               ! for specifying Identity key and & for specifying port number
#
#	Jul 18 2022
#	    Updated codes for handling local commands before running ssh
#	    Updated configuration names 
#	    Code optimization and simplification
#
#      Nov 16 2022
#	   Added option for copy password option (passwords will be taken from passwords file and stored into clipboard for the jumper)
#
#      December 29 2022
#	   Added option only to copy password
#
#      Aug 03 2023
#	   Added option for keeping oath secret token in config file. The oath key generated if the secret is provided
#	   removed copy text true/false flag from config. instead if the password field has any value that will be copied into clipboard.
#	   set terminal '-t' by default,  that can be disabled by "terminal: false"

cd "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

#Check resource files existence 
source libs/_precheck.rc

#Collect configs and options for the sshjumper
source libs/_sshjconfig_collect_configs.rc

#Print description or jumper name 

echo -e "\033[1;32m${sjdesc:=$sshjumper}\033[0m"
###################################################  

#copy specified passsoword or text into the clipboard
if [[ -n "$sjpassword" ]] ;then
	{ echo -n "$sjpassword" | xclip -sel clip ; } && echo -e "\033[1;35mPassword copied. $sjpassword \033[0m";
	[[ -n "$onlycopypass" ]] && exit 0
fi

jumper=""
[[ -z $sjonlyprint ]] && echo -en "\033[1;35mGoing to\033[33m"

#for hop in "${sjhops[@]}"; do
for hop in $sjhops; do

  #collection options
  # & Port 
  # ! Identity key file
  # ? : FUTURE PURPOSE

  SEPARATED=`sed 's/[&!?]/\n&/g' <<< $hop`
  hop=`grep -v "[&!?]" -m1 <<<$SEPARATED`

  port=`grep "&.*" <<<$SEPARATED -m1 | sed 's/^&\(.*\)/\1/'`
  ikey=`grep "!.*" <<<$SEPARATED -m1 | sed 's/^!\(.*\)/\1/'`
  #FUTUREPURPOSE=`grep "?.*" <<< $SEPARATED | sed 's/^?\(.*\)/\1'


  [[ -n "$port" ]] && _port="-p $port"

  if [[ -n "$ikey" ]] ;then
  	[[ "$ikey" = /* ]] || ikey="ssh_keys/$ikey"
	_ikey="-i $ikey"
  fi	
  
  jumper+="ssh $_port $_ikey $sjquiet $sjverbose $sjterminal $sjx11 ${hop[0]} "; 
  [[ -z $sjonlyprint ]] && echo -en " $hop \e[36m$port\033[0m\033[1;33m ->"
done


#Only print ssh line instead of execution, if -i passed						
if [[ -n $sjonlyprint ]];then
       [[ -z	$sjlocalcommand  ]] || echo "Local command : \`$sjlocalcommand\` "; 
       [[ -z	$sjremotecommand ]] || echo "Remote command: \`$sjremotecommand\`"; 

#       echo -en '\033[1A\033[K';
       echo $jumper $sjremotecommand | grep --color=always ssh;
       echo
else

	echo -e "\b\b\b \033[1;34m\$$sjremotecommand\033[0m" 

	#Local command
	[[ -z "$sjlocalcommand" ]] || eval $sjlocalcommand; 
	
	[[ -n "$sjoathsecret" ]] && echo -n "Oauth secret token: " && oathtool --totp -b "$sjoathsecret"

	#Print ssh info in terminal tab
	printf "\e]2;Login to $sshjumper \a"

	#Do ssh / remote command if given
	eval $jumper $sjremotecommand;
	echo "Logged out from $sshjumper" 

	#Clear clip board
	if [[ ! -z "$sjpassword" ]];then
		echo "Copied password is cleared"
	       	xclip -sel clip -i /dev/null
	fi


fi
